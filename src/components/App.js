import Header from "./Header";
import Main from "./Main";
import Footer from "./Footer";
import Offers from "./Offers";
import "./CSS/app.css";

const App = () => {
  return (
    <div id="app">
      <Header />
      <Main />
      <Offers />
      <Footer />
    </div>
  );
};
export default App;
