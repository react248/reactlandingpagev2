import Nav from "./subComponents/Nav";
import HeaderBackground from "./subComponents/HeaderBackground";

const Header = () => {
  return (
    <header>
      <Nav />
      <HeaderBackground />
    </header>
  );
};
export default Header;
