import Specialist from "./subComponents/Specialist";
import "./CSS/main.css";

const employees = [
  {
    name: "Jonny Nowak",
    position: "Senior React Developer",
    about:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi vel massa et lacus egestas cursus a non magna. Fusce scelerisque blandit nunc, id malesuada ex lobortis a. Integer felis nisi, tempor elementum lorem in, varius pellentesque ligula. Duis efficitur lacinia enim, non tincidunt libero ultrices in.",
    key: 0,
    photoUrl: "/specialist-1.jpg",
  },
  {
    name: "Antony Donat",
    position: "Regular .Net Engineer",
    about:
      "Lorem ipsum dolor sisfsdfds, consectetur adipiscing elit. Morbi vel massa et lacus egestas cursus a non magna. Fusce scelerisque blandit nunc, id malesuada ex lobortis a. Integer felis nisi, tempor elementum lorem in, varius pellentesque ligula. Duis efficitur lacinia enim, non tincidunt libero ultrices in.",
    key: 1,
    photoUrl: "/specialist-2.jpg",
  },
];

const Main = () => {
  return (
    <main className="main-section" id="aboutUsSection">
      <div className="main-container">
        <h1 className="title-main">Our specialists</h1>
        {employees.map(({ name, position, about, key, photoUrl }) => (
          <Specialist
            name={name}
            position={position}
            about={about}
            key={key}
            photoUrl={photoUrl}
          />
        ))}
      </div>
    </main>
  );
};
export default Main;
